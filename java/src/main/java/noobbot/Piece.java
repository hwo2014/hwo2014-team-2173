package noobbot;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Bendley on 18.04.14.
 */
public class Piece {
    private double length;
    @SerializedName("switch") private boolean laneSwitch;
    private double radius;
    private double angle;

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public boolean isSswitch() {
        return laneSwitch;
    }

    public void setSswitch(boolean sswitch) {
        this.laneSwitch = sswitch;
    }

    public double getAngle() {
        return angle;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }
}
