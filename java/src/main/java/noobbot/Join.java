package noobbot;

/**
 * Created by Bendley on 19.04.14.
 */
public class Join {
    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }
}
