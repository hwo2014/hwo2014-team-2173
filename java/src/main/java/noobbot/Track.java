package noobbot;

/**
 * Created by Bendley on 18.04.14.
 */
public class Track {
    private String id;
    private String name;
    private Piece[] pieces;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Piece[] getPieces() {
        return pieces;
    }

    public void setPieces(Piece[] pieces) {
        this.pieces = pieces;
    }

}
