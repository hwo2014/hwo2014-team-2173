package noobbot;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Bendley on 18.04.14.
 */
public class Race {
    private Track track;
    private Lane[] lanes;
    private RaceSession raceSession;

    public Track getTrack() {
        return track;
    }

    public void setTrack(Track track) {
        this.track = track;
    }

    public Lane[] getLanes() {
        return lanes;
    }

    public void setLanes(Lane[] lanes) {
        this.lanes = lanes;
    }

    public RaceSession getRaceSession() {
        return raceSession;
    }

    public void setRaceSession(RaceSession raceSession) {
        this.raceSession = raceSession;
    }

    static class Cars{
        @SerializedName("id") Car car;
        Dimensions dimensions;
    }

    static class Lane{
        private double distanceFromCenter;
        private int index;

        public double getDistanceFromCenter() {
            return distanceFromCenter;
        }

        public void setDistanceFromCenter(double distanceFromCenter) {
            this.distanceFromCenter = distanceFromCenter;
        }

        public int getIndex() {
            return index;
        }

        public void setIndex(int index) {
            this.index = index;
        }
    }
}
