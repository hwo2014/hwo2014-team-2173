package noobbot;

/**
 * Created by Bendley on 18.04.14.
 */
public class PiecePosition {
    public int getPieceIndex() {
        return pieceIndex;
    }

    public void setPieceIndex(int pieceIndex) {
        this.pieceIndex = pieceIndex;
    }

    public double getInPieceDistance() {
        return inPieceDistance;
    }

    public void setInPieceDistance(double inPieceDistance) {
        this.inPieceDistance = inPieceDistance;
    }

    public Lane getLane() {
        return lane;
    }

    public void setLane(Lane lane) {
        this.lane = lane;
    }

    private int pieceIndex;
    private double inPieceDistance;
    private Lane lane;

    static class Lane{
        int startLaneIndex;
        int endLaneIndex;
    }
}
