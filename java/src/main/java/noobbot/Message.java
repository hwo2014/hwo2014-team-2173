package noobbot;

/**
 * Created by Bendley on 18.04.14.
 */
public class Message {
    private String msgType;
    private Object data;
    private String gameId;
    private int    gameTick;

    public String getType() {
        return msgType;
    }

    public void setType(String type) {
        this.msgType = type;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public int getGameTick() {
        return gameTick;
    }

    public Message(String msgType, Object data, String gameId, int gameTick) {
        this.msgType = msgType;
        this.gameId = gameId;
        this.gameTick = gameTick;
        this.data = data;
    }

    public Message(String msgType, Object data, int gameTick){this(msgType, data, null, gameTick);}

    public Message(String msgType, String gameId, int gameTick) {
        this(msgType, null, gameId, gameTick);
    }

    public Message(String msgType, int gameTick) {
        this(msgType, null, null, gameTick);
    }

    public Message(String msgType, Object data) { this(msgType,data,null,0); }

    public void setGameTick(int gameTick) {
        this.gameTick = gameTick;
    }
}
