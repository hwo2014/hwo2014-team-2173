package noobbot;

/**
 * Created by Bendley on 19.04.14.
 */
public class GameEnd {

    public Results[] getResults() {
        return results;
    }

    public void setResults(Results[] results) {
        this.results = results;
    }

    public Results[] getBestLaps() {
        return bestLaps;
    }

    public void setBestLaps(Results[] bestLaps) {
        this.bestLaps = bestLaps;
    }

    private Results[] results;
    private Results[] bestLaps;


    static class Results{
        private Car car;
        private Result result;

        public Result getResult() {
            return result;
        }

        public void setResult(Result result) {
            this.result = result;
        }

        public Car getCar() {
            return car;
        }

        public void setCar(Car car) {
            this.car = car;
        }
    }
}
