package noobbot;

/**
 * Created by Bendley on 19.04.14.
 */
public class Ranking {
    public int getOverall() {
        return overall;
    }

    public void setOverall(int overall) {
        this.overall = overall;
    }

    public int getFastestLap() {
        return fastestLap;
    }

    public void setFastestLap(int fastestLap) {
        this.fastestLap = fastestLap;
    }

    private int overall;
    private int fastestLap;
}
