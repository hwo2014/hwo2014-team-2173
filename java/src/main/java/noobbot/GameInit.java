package noobbot;

/**
 * Created by Bendley on 19.04.14.
 */
public class GameInit {
    private Race race;

    public Race getRace() {
        return race;
    }

    public void setRace(Race race) {
        this.race = race;
    }
}
