package noobbot;

/**
 * Created by Bendley on 18.04.14.
 */
public class DNF {
    private Car car;
    private String reason;
    private String gameId;
    private int    gameTick;

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public int getGameTick() {
        return gameTick;
    }

    public void setGameTick(int gameTick) {
        this.gameTick = gameTick;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
