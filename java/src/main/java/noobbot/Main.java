package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;

public class Main {
//    final static String botName = "BremenNerds";
//    final static String botKey = "FrAPG6x7LIvXgQ";
//    final static int port = 8091;
    public int mGameTick = 0;
    public Car mMyCar;

    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, botName, botKey);
    }

    final Gson gson = new Gson();
    private PrintWriter writer;

    public Main(final BufferedReader reader, final PrintWriter writer, final String botName, final String botKey) throws IOException {
        this.writer = writer;
        String line = null;

        join(botName, botKey);

        while ((line = reader.readLine()) != null) {
            final Message msgFromServer = gson.fromJson(line, Message.class);
            mGameTick = msgFromServer.getGameTick();
            switch (msgFromServer.getType()) {
                case "join": {
                    System.out.println("Joined");
                    break;
                }
                case "yourCar": {
                    System.out.println("Your Car");
                    final Car car = gson.fromJson(gson.toJson(msgFromServer.getData()), Car.class);
                    mMyCar = car;
                    System.out.println(car.getName());
                    System.out.println(car.getColor());
                    break;
                }
                case "gameInit": {
                    final Race race = gson.fromJson(gson.toJson(msgFromServer.getData()), GameInit.class).getRace();
                    System.out.println("Race init");
                    System.out.println("Pieces : "+race.getTrack().getPieces().length);
                    break;
                }
                case "gameStart": {
                    System.out.println("Race start");
                    break;
                }
                case "gameEnd": {
                    System.out.println("Race end");
                    ping();
                    break;
                }
                case "carPositions": {
                    CarPosition[] carPositions = gson.fromJson(gson.toJson(msgFromServer.getData()), CarPosition[].class);
                    throttle(0.5);
                    break;
                }
                case "lapFinished": {
                    LapFinished lapFinished = gson.fromJson(gson.toJson(msgFromServer.getData()), LapFinished.class);
                    System.out.println("lap finished" + lapFinished.getLapTime().getMillis());
                    ping();
                    break;
                }
                case "crash": {
                    System.out.println("crash");
                    final Car car = gson.fromJson(gson.toJson(msgFromServer.getData()), Car.class);
                    System.out.println(car.getName());
                    System.out.println(car.getColor());
                    System.out.println(msgFromServer.getGameTick());
                    break;
                }
                case "spawn": {
                    System.out.println("respawn");
                    final Car car = gson.fromJson(gson.toJson(msgFromServer.getData()), Car.class);
                    System.out.println(car.getName());
                    System.out.println(car.getColor());
                    ping();
                    break;
                }
                case "finish": {
                    System.out.println("Finished");
                    ping();
                    break;
                }
                case "dnf": {
                    final DNF dnf = gson.fromJson(gson.toJson(msgFromServer.getData()), DNF.class);
                    System.out.println("disqualified : " + dnf.getReason());
                    break;
                }
                case "tournamentEnd": {
                    System.out.println("tournament end");
                    break;
                }
                default:
                    System.out.println(gson.toJson(msgFromServer));
                    ping();
            }
        }
    }

    private void send(final Message msg) {
        writer.println(gson.toJson(msg));
        writer.flush();
    }

    private void ping() {
        send(new Message("ping", mGameTick));
    }

    private void join(final String name, final String key) {
        send(new Message("join", new Join(name, key)));
    }

    public void throttle(final double throttle) {
        send(new Message("throttle", throttle, mGameTick));
    }

    private void sswitch(String dir) {
        send(new Message("switch", dir, mGameTick));
    }

    private void switchLeft() {
        sswitch("Left");
    }

    private void switchRight() {
        sswitch("Right");
    }
}