package noobbot;

/**
 * Created by Bendley on 19.04.14.
 */
public class RaceSession {
    public int getLaps() {
        return laps;
    }

    public void setLaps(int laps) {
        this.laps = laps;
    }

    public int getMaxLapTimeMs() {
        return maxLapTimeMs;
    }

    public void setMaxLapTimeMs(int maxLapTimeMs) {
        this.maxLapTimeMs = maxLapTimeMs;
    }

    public boolean isQuickRace() {
        return quickRace;
    }

    public void setQuickRace(boolean quickRace) {
        this.quickRace = quickRace;
    }

    private int laps;
    private int maxLapTimeMs;
    private boolean quickRace;
}
