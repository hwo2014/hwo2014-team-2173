package noobbot;

/**
 * Created by Bendley on 19.04.14.
 */
public class LapFinished {
    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public Result getLapTime() {
        return lapTime;
    }

    public void setLapTime(Result lapTime) {
        this.lapTime = lapTime;
    }

    public Result getRaceTime() {
        return raceTime;
    }

    public void setRaceTime(Result raceTime) {
        this.raceTime = raceTime;
    }

    public Ranking getRanking() {
        return ranking;
    }

    public void setRanking(Ranking ranking) {
        this.ranking = ranking;
    }

    Car car;
    Result lapTime;
    Result raceTime;
    Ranking ranking;
}
