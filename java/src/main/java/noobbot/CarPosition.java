package noobbot;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Bendley on 18.04.14.
 */
public class CarPosition {
    @SerializedName("id") private Car car;
    private double angle;
    private PiecePosition piecePosition;
    private int lap;

    public PiecePosition getPiecePosition() {
        return piecePosition;
    }

    public void setPiecePosition(PiecePosition piecePosition) {
        this.piecePosition = piecePosition;
    }

    public int getLap() {
        return lap;
    }

    public void setLap(int lap) {
        this.lap = lap;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public double getAngle() {
        return angle;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }
}
